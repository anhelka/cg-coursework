#include <ModelTriangle.h>
#include <CanvasTriangle.h>
#include <DrawingWindow.h>
#include <Utils.h>
#include <fstream>
#include <vector>
#include <list>
#include <tuple>
// #include <cglm/cglm.h>

using namespace std; 
using namespace glm; 

#define WIDTH 320
#define HEIGHT 240

// Important functions: DO NOT DELETE 
void update();
void draw(); 
void handleEvent(SDL_Event event);
DrawingWindow window = DrawingWindow(WIDTH, HEIGHT, false);
// glm::vec3 functions 
void glm_vec3_sub(vec3 v1, vec3 v2, vec3 result);
void glm_vec3_divs(vec3 v1, float scale, vec3 result);
void glm_vec3_scale(vec3 v1, float scale, vec3 result); 
void glm_vec3_add(vec3 v1, vec3 v2, vec3 result);
// Interpolation functions 
vector<float> interpolate(float from, float to, int nrOfValues);
vector<vec3> interpolateVec(vec3 from, vec3 to, int nrOfValues);
vector<CanvasPoint> interpolateLine(CanvasPoint from, CanvasPoint to);
// Draw functions
void drawLine(CanvasPoint from, CanvasPoint to, Colour colour);
void drawTri(CanvasTriangle triangle, Colour colour);
// Input files functions
vector<Colour> readPPM(string filepath);
vector<ModelTriangle> readOBJ(string filepath);
// Functions that make life easier 
uint32_t cvtColour(Colour colour);
CanvasTriangle sortTri(CanvasTriangle triangle); 
CanvasPoint splitTri(CanvasTriangle triangle);

int main(int argc, char* argv[]) {
  SDL_Event event;
  while(true)
  {
    // We MUST poll for events - otherwise the window will freeze !
    if(window.pollForInputEvents(&event)) handleEvent(event);
    update();
    // Need to render the frame at the end, or nothing actually gets shown on the screen !
    window.renderFrame();
  }
}

void update() {
    // Function for performing animation (shifting artifacts or moving the camera)
}

void handleEvent(SDL_Event event) {
    if (event.type == SDL_KEYDOWN) {
        if (event.key.keysym.sym == SDLK_LEFT) cout << "LEFT" << endl;
        else if (event.key.keysym.sym == SDLK_RIGHT) cout << "RIGHT" << endl;
        else if (event.key.keysym.sym == SDLK_UP) cout << "UP" << endl;
        else if (event.key.keysym.sym == SDLK_DOWN) cout << "DOWN" << endl;
        else if (event.key.keysym.sym == SDLK_c) window.clearPixels(); 
        else if (event.key.keysym.sym == SDLK_ESCAPE) window.destroy(); 
        else if (event.key.keysym.sym == SDLK_p) readPPM("texture.ppm");
    }
    else if(event.type == SDL_MOUSEBUTTONDOWN) cout << "MOUSE CLICKED" << endl;
}

vector<float> interpolate(float from, float to, int nrOfValues) {
  vector<float> output (nrOfValues);
  float step = (to - from) / (nrOfValues-1);
  for (float i = 0; i < nrOfValues; i++)
  {
    output[i] = (from + i * step); 
  }
  return output;
}

vector<CanvasPoint> interpolateLine(CanvasPoint from, CanvasPoint to) {
    float numberOfValues = std::max(abs(to.x-from.x), abs(to.y-from.y)); 
    vector<float> xs = interpolate(from.x, to.x, numberOfValues);
    vector<float> ys = interpolate(from.y, to.y, numberOfValues);
    vector<CanvasPoint> output; 
    for (float i = 0.0; i < numberOfValues; i++) {   
        // window.setPixelColour(round(xs[i]), round(ys[i]), cvtColour(colour)); 
        output[i].x = xs[i];
        output[i].y = ys[i]; 
    }
    return output; 
}

vector<vec3> interpolateVec(vec3 from, vec3 to, int nrOfValues) {
  vector<vec3> output (nrOfValues);
  vec3 step, temp;
  // Initiate step values as a vec3 
  glm_vec3_sub(to, from, temp);
  glm_vec3_divs(temp, (float)(nrOfValues-1), step);
  // Create output vector of vec3s
  for (int i = 0; i < nrOfValues; i++)
  {
    glm_vec3_scale(step, (float)(i), temp);
    glm_vec3_add(from, temp, output[i]);
  }
  return output;
}

// glm_vec3 functions needed: inspired by cglm
void glm_vec3_sub(vec3 v1, vec3 v2, vec3 result) {
    result[0] = v1[0] - v2[0];
    result[1] = v1[1] - v2[1];
    result[2] = v1[2] - v2[2];
}
void glm_vec3_divs(vec3 v1, float scale, vec3 result) {
    result[0] = v1[0] / scale; 
    result[1] = v1[1] / scale; 
    result[2] = v1[2] / scale; 
}
void glm_vec3_scale(vec3 v1, float scale, vec3 result) {
    result[0] = v1[0] * scale; 
    result[1] = v1[1] * scale; 
    result[2] = v1[2] * scale; 
}
void glm_vec3_add(vec3 v1, vec3 v2, vec3 result) {
    result[0] = v1[0] + v2[0];
    result[1] = v1[1] + v2[1];
    result[2] = v1[2] + v2[2];
}

void drawLine(CanvasPoint from, CanvasPoint to, Colour colour) {
    float numberOfValues = std::max(abs(to.x-from.x), abs(to.y-from.y)); 
    vector<CanvasPoint> line = interpolateLine(from, to); 
    for (float i = 0.0; i < numberOfValues; i++) {   
        window.setPixelColour(round(line[i].x), round(line[i].y), cvtColour(colour)); 
    }
}

void drawTri(CanvasTriangle triangle, Colour colour) {
    // drawLine(triangle.vertices[0], triangle.vertices[1], colour);
    // drawLine(triangle.vertices[1], triangle.vertices[2], colour);
    // drawLine(triangle.vertices[0], triangle.vertices[2], colour);

    CanvasPoint fourth = splitTri(triangle);
    float numberOfSteps = fourth.y-triangle.vertices[0].y; 
    vector<CanvasPoint> left = interpolateLine(triangle.vertices[0], fourth);
    vector<CanvasPoint> right = interpolateLine(triangle.vertices[0], triangle.vertices[1]);
    for (float i = 0; i < numberOfSteps; i++) {
        drawLine(left[i], right[i], colour); 
    }
    numberOfSteps = triangle.vertices[2].y - fourth.y;
    left = interpolateLine(triangle.vertices[2], fourth);
    right = interpolateLine(triangle.vertices[2], triangle.vertices[1]);
    for (float i = 0; i < numberOfSteps; i++) {
        drawLine(left[i], right[i], colour); 
    }
}

vector<Colour> readPPM(string filepath) {
  vector<Colour> colours;
  ifstream image(filepath);  
  if (image.is_open()) {
    char magicnumber[100];
    char text[100];
    char dimensions[100];
    char maxColour[100]; 
    image.getline(magicnumber, 100);
    image.getline(text, 100);
    image.getline(dimensions, 100);
    image.getline(maxColour, 100);

    string* dimensions_s = split(dimensions, ' '); 

    int width = std::stoi(dimensions_s[0]);
    int height = std::stoi(dimensions_s[1]);

    vector<int> pixels (width*height*3);

    int counter = 0;
    while (counter < width*height*3) {
      pixels[counter] = image.get(); 
      counter++;
    }

    vector<Colour> colours (pixels.size()/3);
    for (int i = 0; i < (int) pixels.size(); i+=3) {
        colours[i/3].red = pixels[i];
        colours[i/3].green = pixels[i+1];
        colours[i/3].blue = pixels[i+2];
    }

    DrawingWindow newWindow = DrawingWindow(width, height, false);

    for (int row = 0; row < newWindow.height; row++) {
      for (int col = 0; col < newWindow.width; col++) {
        newWindow.setPixelColour(col, row, cvtColour(colours[row*width+col]));
      }
    }
    newWindow.renderFrame();
  } 
  return colours; 
}

uint32_t cvtColour(Colour colour) {
  return (255<<24) + (int(colour.red)<<16) + (int(colour.green)<<8) + int(colour.blue);   
}

CanvasPoint splitTri(CanvasTriangle triangle) {
  CanvasPoint fourth; 
  fourth.y = triangle.vertices[1].y; 
  int a = (triangle.vertices[2].x-triangle.vertices[0].x); 
  int bprime = (triangle.vertices[2].y-triangle.vertices[1].y);
  int b = (triangle.vertices[2].y-triangle.vertices[0].y);
  fourth.x = triangle.vertices[2].x - a*bprime/b;
  return fourth; 
}